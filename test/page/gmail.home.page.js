class GmailHomePage {

    get composeButton (){
        return $("//div[@jscontroller='eIu7Db']") 
    }

    get recipientArea (){
        return $("//textarea[@class='vO']") 
    }
    
    get topicArea (){
        return $("//input[@class='aoT']") 
    }

    get messageArea (){
        return $("//div[@class='Am Al editable LW-avf tS-tW']") 
    }

    get sendButton (){
        return $("//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']") 
    }

    get sentMessageTab (){
        return $("//a[contains(@href,'sent') and @class='J-Ke n0']") 
    }
}

export default new GmailHomePage()