import ElementWait from '../util/element.wiat'; 
import SentMessagePage from '../page/sent.message.page';

class GimailSentMessageBO {

    getSentMsgText() {
        return ElementWait.elementIsDisplayed(SentMessagePage.sentMessage).getText();
    }

    deleteFirstLetter() {
        SentMessagePage.allCheckbox.click();

        ElementWait.clickIfElementIsDisplayed(SentMessagePage.deleteMsg);

        ElementWait.elementIsDisplayed(SentMessagePage.refSendInClearAreaMsg);
    }

}

export default new GimailSentMessageBO();