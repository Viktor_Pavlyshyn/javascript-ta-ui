import GmailLoginPage from '../page/gmail.login.page';
import ElementWiat from '../util/element.wiat';

class GmailLoginBO {

    loginToGmail(login, password){
        let inputLogin = ElementWiat.elementIsDisplayed(GmailLoginPage.setLogin);
        inputLogin.setValue(login);

        GmailLoginPage.submitButtonLogin.click();

        let inputPass = ElementWiat.elementIsDisplayed(GmailLoginPage.setPassword);
        inputPass.setValue(password);
        GmailLoginPage.submitButtonPassword.click();
    }
}

export default new GmailLoginBO();