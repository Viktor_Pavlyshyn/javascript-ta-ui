class ElementWiat {
    elementIsDisplayed(element){
        browser.waitUntil(() => {
            return element.isDisplayed() === true
        }, 15000, "Can't sent keys because elekent is't Displayed.")
        return element;
    }
    
    clickIfElementIsDisplayed(element){
        browser.waitUntil(() => {
            return element.isDisplayed() === true
        }, 15000, "Can't sent keys because elekent is't Displayed.")
        element.click();
    }

}

export default new ElementWiat();