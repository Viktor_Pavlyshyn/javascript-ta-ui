Task:

1. Open gmail & login

2. Click "Compose" button

3. Fill "To", "Subject" & "message" fields

4. Click "send" button

5. Verify that message is in "sent" folder

6. Remove message from the "sent" folder


To run the "TA-hw-AllureReport", you need:

 - run this command: "npm run test".

To get a generated report for "JAVASCRIPT-TA-UI", you need:

 - run this command: "allure generate allure-results/ --clean"; 
 - run commond: "allure open".