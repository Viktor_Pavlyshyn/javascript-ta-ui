import {assert} from 'chai';
import testdata from '../configdata/testdata'
import GmailLoginPage from '../page/gmail.login.page';
import GmailLoginBO from '../businessobject/gmail.login.bo';
import GmailHomeBO from '../businessobject/gmail.home.bo';
import GmailSentMessageBO from '../businessobject/gmail.sent.message.bo';

describe('Verify gmail.', () => {

    before(() => {
        GmailLoginPage.open();
    });

    it('Sign in to Gmail, send a message, check for messages on the sent tab, and then delete the message.', () => {

        GmailLoginBO.loginToGmail(testdata.login, testdata.password);

        const message  = testdata.getRandomInt();

        GmailHomeBO.writeAndSendMessage(testdata.recipient, testdata.topic, message);

        GmailHomeBO.navigateToSentLetter();

        assert.isTrue(GmailSentMessageBO.getSentMsgText().includes(message), 'Wrong text input.')

        GmailSentMessageBO.deleteFirstLetter()
    });
})