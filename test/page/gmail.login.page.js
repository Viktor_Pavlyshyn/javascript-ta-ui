import Page from './page';
import message from '../configdata/testdata'
import testdata from '../configdata/testdata';

class GmailLoginPage extends Page {
    
    get setLogin (){
        return $("//input[@id='identifierId']") 
    }

    get submitButtonLogin (){
        return $("//button[@jsname='LgbsSe']") 
    }

    get setPassword (){
        return $("//input[@name='password']") 
    }

    get submitButtonPassword (){
        return $("//button[@jscontroller='soHxf'][ancestor::div[@class='qhFLie']]") 
    }

    open() {
        super.open(testdata.gmailLogitURL)
    }
}

export default new GmailLoginPage()