import GmailHomePage from '../page/gmail.home.page';
import ElementWiat from '../util/element.wiat';

class GmailHomeBO {

    writeAndSendMessage(recipient, topic, textMessage) {

        ElementWiat.clickIfElementIsDisplayed(GmailHomePage.composeButton);

        ElementWiat.elementIsDisplayed(GmailHomePage.recipientArea).setValue(recipient);

        ElementWiat.elementIsDisplayed(GmailHomePage.topicArea).setValue(topic);

        ElementWiat.elementIsDisplayed(GmailHomePage.messageArea).setValue(textMessage);

        ElementWiat.clickIfElementIsDisplayed(GmailHomePage.sendButton);
    }

    navigateToSentLetter() {
        ElementWiat.clickIfElementIsDisplayed(GmailHomePage.sentMessageTab);
    }
}

export default new GmailHomeBO();